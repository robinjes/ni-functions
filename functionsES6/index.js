import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import {
  texts_infos_get as get_texts_infos,
  texts_contents_get as get_texts_contents,
  text_set as set_save,
} from './texts/rest';

import {
  context_get as get_context,
} from './context/rest';

import {
  login_get as get_login,
  close_set as set_close,
  favorite_set as set_favorite,
} from './users/rest';

/* Init */
admin.initializeApp(functions.config().firebase);

export {
  get_context,
  get_texts_infos,
  get_texts_contents,
  get_login,
  set_close,
  set_favorite,
  set_save
};
