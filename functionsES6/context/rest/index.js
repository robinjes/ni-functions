/* Librairies */
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
/* CORS */
const cors = require('cors')({ origin: true });

/**
 * GET context (database)
 */
export const context_get = functions.https.onRequest((req, res) => {
  if (req.method === 'PUT') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const textsRef = admin.database().ref('/context');
    textsRef
      .once('value')
      .then((snapshot_texts) => {
        res.status(200).send({
          success: 'Context fetched.',
          content: snapshot_texts.val()
        });
      })
      .catch((error) => {
        res.status(403).send({ error: 'Context fetch failed.', more: error });
      });
  });
});
