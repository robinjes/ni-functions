/* Librairies */
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
/* CORS */
const cors = require('cors')({ origin: true });

/**
 * GET texts info (database)
 */
export const texts_infos_get = functions.https.onRequest((req, res) => {
  if (req.method === 'PUT') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const textsRef = admin.database().ref('/texts/infos');
    textsRef
      .once('value')
      .then((snapshot_texts) => {
        res.status(200).send({
          success: 'Texts info fetched.',
          content: snapshot_texts.val()
        });
      })
      .catch((error) => {
        res.status(403).send({ error: 'Texts info fetch failed.', more: error });
      });
  });
});

/**
 * GET texts content (database)
 */
export const texts_contents_get = functions.https.onRequest((req, res) => {
  if (req.method === 'PUT') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const textsRef = admin.database().ref('/texts/contents');
    textsRef
      .once('value')
      .then((snapshot_texts) => {
        res.status(200).send({
          success: 'Texts content fetched.',
          content: snapshot_texts.val()
        });
      })
      .catch((error) => {
        res.status(403).send({ error: 'Texts content fetch failed.', more: error });
      });
  });
});


/**
 * SET texts content (database)
 */
export const text_set = functions.https.onRequest((req, res) => {
  if (req.method === 'GET') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const {
      email,
      title,
      text
    } = req.body;

    let {
      textid,
      type
    } = req.body;

    if (textid && email && /\S+@\S+\.\S+/.test(email)) {
      const uid = email.replace(/@/g, '_').split('.').join('_');
      admin
        .database()
        .ref(`/users/${uid}`)
        .once('value')
        .then((user_snap) => {
          const user_info = user_snap.val();
          const updates = {};
          if (user_snap.exists() && user_info.grp === 'ADMIN') {

            const version = String(+new Date());
            let contentkey = `${textid}_${version}`;

            if (textid === 'NEW') {
              // new text to add
              textid = admin.database().ref('/texts/infos').push().key;
              contentkey = `${textid}_${version}`;
              if (type === 'music_notes') type = 'lyrics';
              if (type === 'notes') type = 'text';
              updates[`/texts/infos/${textid}/id`] = textid;
              updates[`/texts/infos/${textid}/createdAt`] = Number.parseInt(version, 10);
              updates[`/texts/infos/${textid}/versions`] = [version];
              updates[`/texts/infos/${textid}/lastVersion`] = version;
              updates[`/texts/infos/${textid}/type`] = type;
              updates[`/texts/contents/${contentkey}/title`] = title || 'No title.';
              updates[`/texts/contents/${contentkey}/content`] = text || 'New text.';
              admin.database().ref().update(updates);
              res.status(200).send({ success: `Text #${textid} added.`, content: [version] });
            } else {
              admin
                .database()
                .ref(`/texts/infos/${textid}`)
                .once('value')
                .then((infos_snap) => {
                  if (infos_snap.exists()) {
                    const versions = infos_snap.val().versions || [];
                    updates[`/texts/infos/${textid}/versions`] = [...versions, version];
                    updates[`/texts/infos/${textid}/lastVersion`] = version;
                    updates[`/texts/infos/${textid}/type`] = type;
                    updates[`/texts/contents/${contentkey}/title`] = title || 'No title.';
                    updates[`/texts/contents/${contentkey}/content`] = text || '';
                    admin.database().ref().update(updates);
                    res.status(200).send({ success: `Text #${textid} updated.`, content: [...versions, version] });
                  } else {
                    res.status(403).send({ error: `Text #${textid} does not exist.`, content: user_info });
                  }
                })
                .catch((error) => {
                  res.status(403).send({ error: 'Text info request failed.', more: error });
                });
            }
          } else {
            res.status(403).send({ error: `${email} is not allowed to interact.` });
          }
        })
        .catch((error) => {
          res.status(403).send({ error: 'User request failed.', more: error });
        });
    } else {
      res.status(403).send({ error: 'Wrong email format.' });
    }
  });
});

