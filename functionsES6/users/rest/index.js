/* Librairies */
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
/* CORS */
const cors = require('cors')({ origin: true });

/**
 * GET user (database)
 */
export const login_get = functions.https.onRequest((req, res) => {
  if (req.method === 'PUT' || req.method === 'POST') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const {
      email
    } = req.query;

    if (email && /\S+@\S+\.\S+/.test(email)) {
      const uid = email.replace(/@/g, '_').split('.').join('_');
      admin
        .database()
        .ref(`/users/${uid}`)
        .once('value')
        .then((snap) => {
          if (snap.exists()) {
            // already exists
            res.status(200).send({
              success: 'Session fetched.',
              content: snap.val()
            });
          } else {
            // create user
            const content = {
              grp: 'VISITOR',
              id: uid,
              lovedTexts: [],
              shatTexts: []
            };
            const updates = {};

            updates[`/users/${uid}`] = content;
            admin.database().ref().update(updates);
            res.status(200).send({
              success: 'User created.',
              content
            });
          }

        })
        .catch((error) => {
          res.status(403).send({ error: 'Request failed.', more: error });
        });
    } else {
      res.status(403).send({ error: 'Wrong email format.' });
    }
  });
});

/**
 * SET close (database)
 */
export const close_set = functions.https.onRequest((req, res) => {
  if (req.method === 'GET') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const {
      email,
      textid
    } = req.body;

    if (textid && email && /\S+@\S+\.\S+/.test(email)) {
      const uid = email.replace(/@/g, '_').split('.').join('_');
      admin
        .database()
        .ref(`/users/${uid}`)
        .once('value')
        .then((user_snap) => {
          const updates = {};
          if (user_snap.exists()) {
            admin
              .database()
              .ref(`/texts/infos/${textid}`)
              .once('value')
              .then((infos_snap) => {
                if (infos_snap.exists()) {
                  const shits = infos_snap.val().shits || 0;
                  updates[`/texts/infos/${textid}/shits`] = shits + 1;
                  const shatTexts = user_snap.val().shatTexts || [];
                  if (shatTexts.indexOf(textid) === -1) shatTexts.push(textid.toString());
                  updates[`/users/${uid}/shatTexts`] = shatTexts;
                  admin.database().ref().update(updates);
                  res.status(200).send({ success: `Favorite set for text #${textid}`, content: { ...infos_snap.val(), shits: shits + 1 } });
                } else {
                  res.status(403).send({ error: `Text #${textid} does not exist.`, content: user_snap.val() });
                }
              })
              .catch((error) => {
                res.status(403).send({ error: 'Text info request failed.', more: error });
              });
          } else {
            res.status(403).send({ error: `${email} is not allowed to interact.` });
          }
        })
        .catch((error) => {
          res.status(403).send({ error: 'User request failed.', more: error });
        });
    } else {
      res.status(403).send({ error: 'Wrong email format.' });
    }
  });
});

/**
 * SET favorite (database)
 */
export const favorite_set = functions.https.onRequest((req, res) => {
  if (req.method === 'GET') {
    return res.status(403).send('Forbidden!');
  }

  return cors(req, res, () => {
    const {
      email,
      textid
    } = req.body;

    if (textid && email && /\S+@\S+\.\S+/.test(email)) {
      const uid = email.replace(/@/g, '_').split('.').join('_');
      admin
        .database()
        .ref(`/users/${uid}`)
        .once('value')
        .then((user_snap) => {
          const updates = {};
          if (user_snap.exists()) {
            admin
              .database()
              .ref(`/texts/infos/${textid}`)
              .once('value')
              .then((infos_snap) => {
                if (infos_snap.exists()) {
                  const likes = infos_snap.val().likes || 0;
                  updates[`/texts/infos/${textid}/likes`] = likes + 1;
                  const lovedTexts = user_snap.val().lovedTexts || [];
                  if (lovedTexts.indexOf(textid) === -1) lovedTexts.push(textid.toString());
                  updates[`/users/${uid}/lovedTexts`] = lovedTexts;
                  admin.database().ref().update(updates);
                  res.status(200).send({ success: `Favorite set for text #${textid}`, content: { ...infos_snap.val(), likes: likes + 1 } });
                } else {
                  res.status(403).send({ error: `Text #${textid} does not exist.`, content: user_snap.val() });
                }
              })
              .catch((error) => {
                res.status(403).send({ error: 'Text info request failed.', more: error });
              });
          } else {
            res.status(403).send({ error: `${email} is not allowed to interact.` });
          }
        })
        .catch((error) => {
          res.status(403).send({ error: 'User request failed.', more: error });
        });
    } else {
      res.status(403).send({ error: 'Wrong email format.' });
    }
  });
});

