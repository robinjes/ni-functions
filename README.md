# About
Firebase backend for 'Natural Intelligence' project.
# Run project
  npm install
  npm install -g firebase-tools
  ## If you want to debug a cloud function locally :
    npm install -g @google-cloud/functions-emulator
  ## If buidling for the first time, or after adding a new package :
  npm run deploy-start
  ## Otherwise :
  npm run deploy

# API
GET / Context :
https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_context

GET / Texts info :
https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_texts_infos

GET / Texts content :
https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_texts_contents

# Ressources
- [Choose a Database: Cloud Firestore or Realtime Database](https://firebase.google.com/docs/database/rtdb-vs-firestore)
- [Perform simple and compound queries in Cloud Firestore](https://firebase.google.com/docs/firestore/query-data/queries)